package com.smartsheet.samples;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Roland Schablik
 */
public class JDBCConn {

    private static final String CON = "jdbc:mysql://...";
    private static final String USR = "X";
    private static final String PSW = "X";
    private ArrayList<String> nameList = new ArrayList<>();
    private ArrayList<Long> idList = new ArrayList<>();

    public ArrayList<String> getNameList() {
        return nameList;
    }

    public void setNameList(ArrayList<String> nameList) {
        this.nameList = nameList;
    }

    public ArrayList<Long> getIdList() {
        return idList;
    }

    public void setIdList(ArrayList<Long> idList) {
        this.idList = idList;
    }




    public void getSheetName() throws SQLException {

        try (Connection con = DriverManager.getConnection(CON, USR, PSW);
                Statement statement = con.createStatement()) {

            String query = "SELECT sheetname FROM X";
            ResultSet rs = statement.executeQuery(query);
            while (rs.next()) {
                String name = rs.getString("sheetname");
                this.nameList.add(name);
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

    }
    public void getSheetId() throws SQLException {
        
        try (Connection con = DriverManager.getConnection(CON, USR, PSW);
                Statement statement = con.createStatement()) {

            String query = "SELECT sheetid FROM X";
            ResultSet rs = statement.executeQuery(query);
            while (rs.next()) {
                Long id = rs.getLong("sheetid");
                this.idList.add(id);
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

    }
}
