package com.smartsheet.samples;

import com.smartsheet.api.Smartsheet;
import com.smartsheet.api.SmartsheetBuilder;
import com.smartsheet.api.SmartsheetException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author Roland Schablik
 */
public class DownloadSheet {

    public void convert(ArrayList<Long> sheetId, ArrayList<String> sheetName)
            throws IOException, SmartsheetException {
        
        String token = "X";
        
        for (int i = 0; i < sheetId.size(); i++) {

            File file = new File("C:\\Users\\User\\Desktop\\",
                    sheetName.get(i) + ".XLS");

            if (!file.exists()) {
                file.createNewFile();
            }

            FileOutputStream outputStream = new FileOutputStream(file, false);

            Smartsheet smartsheet = new SmartsheetBuilder().setAccessToken(token).build();

            smartsheet.sheetResources().getSheetAsExcel(sheetId.get(i), outputStream);

            outputStream.flush();
            outputStream.close();
        }
    }
}
