package com.smartsheet.samples;

import com.smartsheet.api.SmartsheetException;
import java.io.IOException;
import java.sql.SQLException;

/**
 *
 * @author Roland Schablik
 */
public class Main {

    public static void main(String[] args) throws SQLException, IOException, SmartsheetException {

        JDBCConn jdbcconn = new JDBCConn();
        DownloadSheet downloadSheet = new DownloadSheet();

        jdbcconn.getSheetName();
        jdbcconn.getSheetId();
        
        downloadSheet.convert(jdbcconn.getIdList(), jdbcconn.getNameList());
    }
}
