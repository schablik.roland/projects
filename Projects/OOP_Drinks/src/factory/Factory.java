package factory;

import alcohol.Alcohol;
import alcohol.AlcoholType;
import alcohol.French;
import alcohol.German;
import alcohol.Hungarian;
import producer.Producer;

public class Factory {

    public static Alcohol create(String alcoholType, String producer, String price, String alcRate, String specialType) {

        AlcoholType alcType = AlcoholType.valueOf(alcoholType);
        int productPrice = Integer.parseInt(price);
        int alcoholRate = Integer.parseInt(alcRate);

        switch (specialType) {
            case "hu":
                return new Hungarian(checkProductType(alcoholType), new Producer(producer), productPrice, alcoholRate);
            case "de":
                return new German(checkProductType(alcoholType), new Producer(producer), productPrice, alcoholRate);
            case "fr":
                return new French(checkProductType(alcoholType), new Producer(producer), productPrice, alcoholRate);
        }

        throw new IllegalArgumentException("WRONG INPUT");
    }

    public static AlcoholType checkProductType(String s) {
        switch (s) {
            case "BEER":
                return AlcoholType.BEER;
            case "WINE":
                return AlcoholType.WINE;
            case "SHOT":
                return AlcoholType.SHOT;

        }
        return null;
    }
}
