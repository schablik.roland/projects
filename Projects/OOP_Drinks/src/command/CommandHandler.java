package command;

import alcohol.Alcohol;
import factory.Factory;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import repository.RepoView;
import repository.Repository;

public class CommandHandler {

    Repository repo = new Repository();
    File itemList = new File("items");
    Factory techItemsFactory = new Factory();
    private static int i = 1;

    public void read() {

        try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            String line;
            i++;
            while ((line = br.readLine()) != null && !"exit".equals(line)) {
                parse(line);
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

    }

    public void parse(String line) {
        String[] comm = line.split(" ");

        if (comm.length > 1 && "CREATE".equals(comm[0])) {
            if (i > 100) {
                System.out.println("Program terminated");
            } else {
                Alcohol alcohol = Factory.create(comm[1], comm[2], comm[3], comm[4], comm[5]);
                repo.addProduct(alcohol);
                i++;
                
            }

        } else if (comm.length == 1 && "SAVE".equals(comm[0])) {
            seriOut(itemList, repo);
            Report report = new Report(repo);
            report.numberOfProducts();
            report.numberOfWines();
            report.producers();

        } else if (comm.length == 1 && "LOAD".equals(comm[0])) {
            seriIn(itemList);

        } else if (comm.length == 1 && "LIST".equals(comm[0])) {
            System.out.println("Termékek listája:");
            RepoView.printList(repo.getRepository());
        }
    }

    public void seriOut(File itemList, Repository repo) {
        try (FileOutputStream fileOutputStream = new FileOutputStream(itemList);
                ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);) {

            objectOutputStream.writeObject(repo);

        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Serialized...");
    }

    public void seriIn(File itemList) {
        try (FileInputStream fileInputStream = new FileInputStream(itemList);
                ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);) {

            repo = (Repository) objectInputStream.readObject();
            System.out.println(repo.toString());

        } catch (ClassNotFoundException | IOException e) {
            e.printStackTrace();
        }
    }
}
