package command;

import alcohol.Alcohol;
import alcohol.AlcoholType;
import alcohol.Hungarian;
import alcohol.French;
import alcohol.German;
import java.util.ArrayList;
import java.util.List;
import repository.Repository;


public class Report {

    private final Repository repository;

    public Report(Repository repository) {
        this.repository = repository;
    }

    public void numberOfProducts() {
        int hu = 0;
        int fr = 0;
        int de = 0;
        int sum = 0;
        
        List<Alcohol> list = repository.getRepository();
        
        for (Alcohol product : list) {
            if (product instanceof Hungarian) {
                hu++;
            } else if (product instanceof French) {
                fr++;
            } else if (product instanceof German) {
                de++;
            }
            sum = hu + fr + de;
        }
        System.out.println("Number of Hungarian products: " + hu);
        System.out.println("Number of French products: " + fr);
        System.out.println("Number of German products: " + de);
        System.out.println("Total number of products: " + sum);
    }

    public void producers() {
        List<Alcohol> list = repository.getRepository();
        List producers = new ArrayList();

        for (Alcohol list1 : list) {
            producers.add(list1.getProducer());
            
        }
        System.out.println("Producers: " + producers);
    }

    public void numberOfWines() {
        int wine = 0;
        
        List<Alcohol> list = repository.getRepository();
        
        for (Alcohol list1 : list) {
            if (list1.getAlcoholType().equals(AlcoholType.WINE)) {
                wine++;
            }
        }
        System.out.println("Nr. of wines: " + wine);
    }
}
