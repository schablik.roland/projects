package alcohol;

import java.io.Serializable;
import java.util.Objects;
import producer.Producer;

public abstract class Alcohol implements Serializable {
    AlcoholType alcoholType;
    Producer producer;
    int price;
    int alcoholRate;
    int barCode;

    public Alcohol(AlcoholType alcoholType, Producer producer, int price, int alcoholRate) {
        this.alcoholType = alcoholType;
        this.producer = producer;
        this.price = price;
        this.alcoholRate = alcoholRate;
        this.barCode = (int) (Math.random()*100000+1);
    }

        
   
    public AlcoholType getAlcoholType() {
        return alcoholType;
    }

    public void setAlcoholType(AlcoholType alcoholType) {
        this.alcoholType = alcoholType;
    }

    public Producer getProducer() {
        return producer;
    }

    public void setProducer(Producer producer) {
        this.producer = producer;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getAlcoholRate() {
        return alcoholRate;
    }

    public void setAlcoholRate(int alcoholRate) {
        this.alcoholRate = alcoholRate;
    }

    public int getBarCode() {
        return barCode;
    }

    public void setBarCode(int barCode) {
        this.barCode = barCode;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.alcoholType);
        hash = 53 * hash + Objects.hashCode(this.producer);
        hash = 53 * hash + this.price;
        hash = 53 * hash + this.alcoholRate;
        hash = 53 * hash + this.barCode;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Alcohol other = (Alcohol) obj;
        if (this.price != other.price) {
            return false;
        }
        if (this.alcoholRate != other.alcoholRate) {
            return false;
        }
        if (this.barCode != other.barCode) {
            return false;
        }
        if (this.alcoholType != other.alcoholType) {
            return false;
        }
        if (!Objects.equals(this.producer, other.producer)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "alcoholType: " + alcoholType + " / producer: " + producer + " / price: " + price + " / alcoholRate: " + alcoholRate + " / barCode: " + barCode;
    }

    
    
    
}
