package alcohol;

import feature.Dilute;
import producer.Producer;
import feature.Drinkable;

/**
 *
 * @author User
 */
public class Hungarian extends Alcohol implements Drinkable, Dilute {

    public Hungarian(AlcoholType alcoholType, Producer producer, int price, int alcoholRate) {
        super(alcoholType, producer, price, alcoholRate);
    }


    @Override
    public void canDrink() {
        System.out.println("iszik");
    }

    @Override
    public void dilute() {
        System.out.println("higít");
    }
    
}
