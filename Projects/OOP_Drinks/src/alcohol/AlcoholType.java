package alcohol;

public enum AlcoholType {
    BEER, WINE, SHOT;
}
