package alcohol;

import exception.TransportableException;
import feature.Freezable;
import feature.Transportable;
import producer.Producer;

public class French extends Alcohol implements Freezable,Transportable {

    private boolean freezed;
    
    public French(AlcoholType alcoholType, Producer producer, int price, int alcoholRate) {
        super(alcoholType, producer, price, alcoholRate);
    }
    

    @Override
    public void freezable() {
        freezed = true;
    }

    @Override
    public void transportable() {
        if (freezed) {
            throw new TransportableException("is NOT transportable");
        } else {
            System.out.println("is transportable");
        }
    }
    
}
