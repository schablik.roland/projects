package repository;

import alcohol.Alcohol;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Repository implements Serializable{

    private final List<Alcohol> repository;

    public Repository() {
        this.repository = new ArrayList<>();
    }

    public List<Alcohol> getRepository() {
        return repository;
    }

    public void addProduct(Alcohol product) {
        repository.add(product);
    }

    @Override
    public String toString() {
        return "Repository{" + "repository=" + repository + '}';
    }
    
    
}
