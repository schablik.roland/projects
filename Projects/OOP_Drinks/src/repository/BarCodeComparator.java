package repository;

import alcohol.Alcohol;
import java.util.Comparator;

public class BarCodeComparator implements Comparator<Alcohol> {

    @Override
    public int compare(Alcohol o1, Alcohol o2) {
        return o1.getBarCode() - o2.getBarCode();

    }

}