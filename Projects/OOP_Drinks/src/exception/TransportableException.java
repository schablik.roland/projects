package exception;

/**
 *
 * @author User
 */
public class TransportableException extends RuntimeException {

    public TransportableException(String message) {
        super(message);
    }

}
