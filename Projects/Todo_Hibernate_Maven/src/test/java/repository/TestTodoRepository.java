package repository;

import com.mycompany.mavenproject1.model.Todo;
import com.mycompany.mavenproject1.repository.TodoRepository;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.PersistenceException;
import org.junit.jupiter.api.Assertions;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class TestTodoRepository {

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    TodoRepository todoRepository;

    List<Todo> filteredTodoList;

    Todo testTodo;

    @BeforeEach
    public void setupFilteredTodoList() {
        filteredTodoList = new ArrayList<>();
        Todo todo1 = new Todo();
        todo1.setDescription("Descr");
        todo1.setSummary("Sums");
        Todo todo2 = new Todo();
        todo2.setDescription("Descr2");
        todo2.setSummary("Sums2");
        filteredTodoList.add(todo1);
        filteredTodoList.add(todo2);
    }

    @BeforeEach
    public void setupTestTodo() {
        testTodo = new Todo();
        testTodo.setDescription("leiras");
        testTodo.setSummary("osszegzes");
    }

    //"Sums", "Descr", null
//    @Test

//    public void testTodoRepo() {
//        assertAll("TestTodoRepository",
//                () -> {
//                    when(todoRepository.searchTodo("Sums", "Descr", null)).thenReturn(filteredTodoList);
//
//                    assertAll("TestReturnedEntities",
//                            () -> {
//                                assertEquals(2, todoRepository.searchTodo("Sums", "Descr", null).size());
//                            },
//                            () -> {
//                                assertEquals(1, todoRepository.searchTodo("Sums", "Descr", null)
//                                        .stream().filter(todo -> "Descr"
//                                        .equals(todo.getDescription()) && "Sums"
//                                        .equals(todo.getSummary())).count());
//                            });
//
//                },
//                () -> {
//                    when(todoRepository.getEm().find(Todo.class, 5L)).thenReturn(testTodo);
//                    
//                    Assertions.assertTrue("leiras".equals(todoRepository.getEm().find(Todo.class, 5L).getDescription()));
//                },
//                () -> {
//                when(todoRepository.getEm().find(Todo.class, -1L)).thenThrow(PersistenceException.class);
//                
//                assertThrows(PersistenceException.class);
//                () -> {
//                    todoRepository.getEm().find(Todo.class, -1L);
//                }
//                        ;
//        );
//
//    }
//
//}
}