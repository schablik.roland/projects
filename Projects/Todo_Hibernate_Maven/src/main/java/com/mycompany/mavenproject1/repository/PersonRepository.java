package com.mycompany.mavenproject1.repository;

import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import com.mycompany.mavenproject1.model.Person;

@Stateless
@LocalBean
public class PersonRepository {

    @PersistenceContext(unitName = "com.mycompany_mavenproject1_war_1.0-SNAPSHOTPU")
    EntityManager em;

    String found = "Select ? from Person";

    public List<Person> getPerson() {
        Query q = em.createQuery("Select g from Person g", Person.class);
        return q.getResultList();
    }

    @Transactional
    public void addPerson(Person person) {
        em.persist(person);
    }

    public List<Person> getSelectedPerson() {
        Query q = em.createQuery("Select g from Person g", Person.class);
        return q.getResultList();
    }

    public Person getPersonByName(String name) {
        TypedQuery<Person> query = em.createQuery(
                "SELECT c FROM Person c WHERE c.name = :name", Person.class);
        return query.setParameter("name", name).getSingleResult();
    }
}
