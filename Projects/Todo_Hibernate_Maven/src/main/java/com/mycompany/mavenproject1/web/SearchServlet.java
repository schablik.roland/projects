package com.mycompany.mavenproject1.web;

import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.mycompany.mavenproject1.repository.TodoRepository;

@WebServlet(name = "SearchServlet", urlPatterns = {"/search"})
public class SearchServlet extends HttpServlet {

    @EJB
    TodoRepository todoRepository;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.getRequestDispatcher("Search.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String desc = request.getParameter("description");
        String sum = request.getParameter("summary");
        String personName = request.getParameter("person");

        request.getSession().setAttribute("searchByUserName", todoRepository.searchTodo(sum, desc, personName));
        System.out.println(todoRepository.searchTodo(sum, desc, personName));
        
        response.sendRedirect(response.encodeRedirectURL(request.getRequestURI()));


    }
}
