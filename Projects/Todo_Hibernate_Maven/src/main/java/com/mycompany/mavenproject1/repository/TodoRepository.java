package com.mycompany.mavenproject1.repository;

import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import com.mycompany.mavenproject1.model.Todo;

@LocalBean
@Stateless
public class TodoRepository {

    @PersistenceContext(unitName = "com.mycompany_mavenproject1_war_1.0-SNAPSHOTPU")
    EntityManager em;

    public List<Todo> getTodos() {
        Query q = em.createQuery("Select g from Todo g", Todo.class);
        return q.getResultList();
    }

    @Transactional
    public void addTodo(Todo todo) {
        em.persist(todo);
    }

    public EntityManager getEm() {
        return em;
    }
    
    

    public List<Todo> searchTodo(String sum, String descr, String name) {
        Query q1 = em.createQuery(
        "select c FROM Todo c WHERE c.summary LIKE :summary AND c.description LIKE :description AND c.person.name LIKE :username", Todo.class);
        q1.setParameter("summary", "%" + sum + "%");
        q1.setParameter("description", "%" + descr + "%");
        q1.setParameter("username", "%" + name + "%");
        return q1.getResultList();
    }
}
