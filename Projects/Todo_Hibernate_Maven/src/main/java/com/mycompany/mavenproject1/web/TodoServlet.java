package com.mycompany.mavenproject1.web;

import java.io.IOException;
import java.util.Optional;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.mycompany.mavenproject1.model.Todo;
import com.mycompany.mavenproject1.model.Person;
import com.mycompany.mavenproject1.repository.TodoRepository;
import com.mycompany.mavenproject1.repository.PersonRepository;

@WebServlet(name = "TodoServlet", urlPatterns = {"/todos"})
public class TodoServlet extends HttpServlet {

    @EJB
    TodoRepository todoRepository;

    @EJB
    PersonRepository personRepository;
    
//    Dao todoDao = new TodoDao();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.setAttribute("todoList", todoRepository.getTodos());
        
        request.setAttribute("personList", personRepository.getPerson());
        
        request.getRequestDispatcher("/todo.jsp").forward(request, response);
        
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String desc = request.getParameter("description");
        String sum = request.getParameter("summary");

        Todo todo = new Todo();
        todo.setSummary(sum);
        todo.setDescription(desc);

        String personName = request.getParameter("person");
        Optional<Person> foundPerson = personRepository
                .getPerson()
                .stream()
                .filter(p -> p.getName()
                .equals(personName))
                .findFirst();
        
        if (!foundPerson.isPresent()) {
            Person person = new Person();
            person.setName(personName);
            personRepository.addPerson(person);
            todo.setPerson(person);
        } else {
            todo.setPerson(foundPerson.get());
        }
        todoRepository.addTodo(todo);

        response.sendRedirect(response.encodeRedirectURL(request.getRequestURI()));


    }
    
    
}