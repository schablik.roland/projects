<%-- 
    Document   : Search
    Created on : 2019.06.15., 11:45:40
    Author     : User
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Search Page</title>

        <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

    </head>
    <body>
        <form class="form-horizontal" action='' method = "post">  
            <fieldset>
                <div id="legend">
                    <legend class="">Search</legend>
                </div>

                <div class="control-group">

                    <label class="control-label"  for="summary">Summary</label>
                    <div class="controls">
                        <input type="summary"  name="summary" placeholder="" class="input-xlarge">
                    </div>
                </div>

                <div class="control-group">

                    <label class="control-label"  for="description">Description</label>
                    <div class="controls">
                        <input type="description" name="description" placeholder="" class="input-xlarge">
                    </div>
                </div>

                <div class="control-group">

                    <label class="control-label"  for="person">Person</label>
                    <div class="controls">
                        <input type="person" id="username" name="person" placeholder="" class="input-xlarge">
                    </div>
                </div>
            </fieldset>

            <form method = "post">  
                <button type="submit" class="btn btn-primary">Search</button>
            </form>

            <a href="todos">
                <button type="submit" class="btn btn-primary">Back to main page</button>
            </a>

            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">SUMMARY</th>
                        <th scope="col">DESCRIPTION</th>
                        <th scope="col">PERSON</th>
                    </tr>
                </thead>
            <c:forEach items="${searchByUserName}" var="Todo"> 
                <tr>
                    <td>${Todo.id}</td>
                    <td>${Todo.summary}</td>
                    <td>${Todo.description}</td>
                    <td>${Todo.person.name}</td>
                </tr>
            </c:forEach>
        </table>
    </body>
</html>