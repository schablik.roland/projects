<%-- 
    Document   : todo
    Created on : 2019.06.15., 11:46:28
    Author     : User
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registration page</title>

        <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

    </head>
    
    <body>

        <form class="form-horizontal" action='' method = "post">  
            <fieldset>

                <div id="legend">
                    <legend class="">Register</legend>
                </div>

                <div class="control-group">

                    <label class="control-label"  for="summary">Summary</label>
                    <div class="controls">
                        <input type="summary"  name="summary" placeholder="" class="input-xlarge">
                        <!--<p class="help-block">Username can contain any letters or numbers, without spaces</p>-->
                    </div>
                </div>

                
                <div class="control-group">

                    <label class="control-label"  for="description">Description</label>
                    <div class="controls">
                        <input type="description" name="description" placeholder="" class="input-xlarge">
                    </div>
                </div>

<!--                <div class="control-group">

                    <label class="control-label"  for="description">Person</label>
                    <div class="controls">
                        <input type="person" name="person" placeholder="" class="input-xlarge">
                    </div>
                </div>-->
                
            <select name="person">
                <c:forEach items="${personList}" var="person"> 
                    <option value="${person.name}">    
                        ${person.name}
                    </option>
                </c:forEach>
            </select>
            </fieldset>
            
            
            <!--<select class="form-control"  id='personList' name='personList' >-->

<!-- ALAP REGFORM            
                        Description <input type="description" name="description"/><br><br>
                        Summary <input type="summary" name="summary"/><br><br>
                        Person <input type="person" name="person"/><br><br>
                        <input type="submit" name="button" value="Create"/>
-->
            <button type="submit" class="btn btn-primary">Create</button>
        </form>

        <a href="search">
            <button type="submit" class="btn btn-primary">
                Search
            </button>
            
<!--            <button>
                Search
            </button>
-->

        </a>

        <table class="table">

            <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">SUMMARY</th>
                    <th scope="col">DESCRIPTION</th>
                </tr>
            </thead>

            <c:forEach items="${todoList}" var="Todo"> 
                <tr>
                    <td>${Todo.id}</td>
                    <td>${Todo.summary}</td>
                    <td>${Todo.description}</td>
                </tr>
            </c:forEach>
        </table>
        <br>
        
        <table class="table">

            <thead>
                <tr>
                    <th scope="col">USER</th>
                </tr>
            </thead>

            <c:forEach items="${personList}" var="Person"> 
                <tr>
                    <td>${Person.name}</td>
                </tr>
            </c:forEach>
        </table>
        





        <!---EREDETI -->

<!--
        <table>
            <tr>
                <td>ID</td>
                <td>SUMMARY</td>
                <td>DESCRIPTION</td>
            </tr>
            <c:forEach items="${todoList}" var="Todo"> 
                <tr>
                    <td>${Todo.id}</td>
                    <td>${Todo.summary}</td>
                    <td>${Todo.description}</td>
                </tr>
            </c:forEach>
        </table>
        <br>
        <table>
            <tr>
                <td>USER</td>
            </tr>
            <c:forEach items="${personList}" var="Person"> 
                <tr>
                    <td>${Person.name}</td>
                </tr>
            </c:forEach>
        </table>
--->
    </body>
</html>

