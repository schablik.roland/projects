package castle.comparable;

import java.io.File;

public class Command {

    Repository repo = new Repository();
    File castleList = new File("castles");
   
    public void parse(String line) {
        String[] comm = line.split(" ");

        if (comm.length > 1 && "add".equals(comm[0])) {
            String city = comm[1];
            String builder = comm[2] + " " + comm[3];
            String country = comm[4];
            Integer rooms = Integer.parseInt(comm[5]);
            Boolean ip;
            if (comm[6].equalsIgnoreCase("0")) {
                ip = false;
            } else {
                ip = true;
            }
            repo.addCastle(new Castle(city, builder, country, rooms, ip));
            System.out.println("Hozzáadva");
            
        } else if (comm.length == 1 && "list".equals(comm[0])) {
            System.out.println("Kastélyok listája:");
            CastleView.printList(repo.getObject());
            
        } else if (comm.length == 1 && "out".equals(comm[0])) {
            MySerializable.seriOut(castleList, repo);
            
        } else if (comm.length == 1 && "in".equals(comm[0])) {
            MySerializable.seriIn(castleList);
        }
    }
}
