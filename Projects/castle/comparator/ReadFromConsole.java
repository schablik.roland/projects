package castle.comparator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ReadFromConsole {

//    public ReadFromConsole() {
//    } // mivel nem példányosítom

    public static void read() {
        Command parser = new Command();

        try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            String line;
            while ((line = br.readLine()) != null && !"exit".equals(line)) {
                parser.parse(line);
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
