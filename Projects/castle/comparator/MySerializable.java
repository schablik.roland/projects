package castle.comparator;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.SortedSet;

public class MySerializable {

    public MySerializable() {
    }

    public static void seriOut(File castleList, Repository repo) {
        try (FileOutputStream fileOutputStream = new FileOutputStream(castleList);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);){

            System.out.println("Adatok kimentve...");
            objectOutputStream.writeObject(repo.repo);

//            objectOutputStream.close(); //try with resources miatt nem kell
//            fileOutputStream.close(); // close mindig finallly (try-with resources)

        } catch (IOException e) { 
            e.printStackTrace();
        }
    }

    public static void seriIn(File castleList) {
        try (FileInputStream fileInputStream = new FileInputStream(castleList);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);) {

            SortedSet<Castle> savedCastle = (SortedSet<Castle>) objectInputStream.readObject(); // ITT VOLT A HIBA
            System.out.println(savedCastle.toString());

//            objectInputStream.close(); //try with resources miatt nem kell
//            fileInputStream.close();

        } catch (ClassNotFoundException | IOException e) { 
            e.printStackTrace();
        }
    }
}
