package castle.comparator;

import java.util.SortedSet;
import java.util.TreeSet;

public class Repository {

    SortedSet<Castle> repo = new TreeSet<>(new CastleCityComparator());

    public void addCastle(Castle a) {
        repo.add(a);
    }
  
    public void getIndexOf(Castle a) {
        repo.headSet(a).size();
    }

    public SortedSet<Castle> getRepo() {
        return repo;
    }
    
    public SortedSet<Castle> getRepoByCountry() {
        SortedSet<Castle> repo2 = new TreeSet<>(new CastleCountryComparator());
        repo2.addAll(repo);
        return repo2;
    }

    @Override
    public String toString() {
        return "Repository\n" + repo;
    }
}
