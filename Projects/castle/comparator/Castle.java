package castle.comparator;

import java.util.Objects;
import java.io.Serializable;
import java.util.Comparator;

public class Castle implements Serializable {

    private static int q = 0;

    private String city;
    private String builder;
    private String country;
    private int nrOfRooms;
    private boolean isPrivate;
    private int id;

    public Castle() {
    }

    public Castle(String city, String builder, String country, int nrOfRooms, boolean isPrivate) {
        this.city = city;
        this.builder = builder;
        this.country = country;
        this.nrOfRooms = nrOfRooms;
        this.isPrivate = isPrivate;
        this.id = ++q;
    }

//    @Override
//    public int compareTo(Castle o) {
//        return this.getId() - o.getId();
//    }
//    @Override
//    public int compare(Castle o1, Castle o2) {
//        return o1.getCity().compareTo(o2.getCity());
//    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getBuilder() {
        return builder;
    }

    public void setBuilder(String builder) {
        this.builder = builder;
    }

    public String getCountry() {
        return country;
    }

    public void setCoutnry(String coutnry) {
        this.country = coutnry;
    }

    public int getNrOfRooms() {
        return nrOfRooms;
    }

    public void setNrOfRooms(int nrOfRooms) {
        this.nrOfRooms = nrOfRooms;
    }

    public boolean isIsPrivate() {
        return isPrivate;
    }

    public void setIsPrivate(boolean isPrivate) {
        this.isPrivate = isPrivate;
    }

//    public int getCounter() {
//        return counter;
//    }
//
//    public static void setCounter(int counter) {
//        Castle.counter = counter;
//    }
//    @Override
//    public int hashCode() {
//        return Objects.hash(id);
//    }
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 17 * hash + Objects.hashCode(this.city);
        hash = 17 * hash + Objects.hashCode(this.builder);
        hash = 17 * hash + Objects.hashCode(this.country);
        hash = 17 * hash + this.nrOfRooms;
        hash = 17 * hash + (this.isPrivate ? 1 : 0);
        hash = 17 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Castle other = (Castle) obj;
        if (this.nrOfRooms != other.nrOfRooms) {
            return false;
        }
        if (this.isPrivate != other.isPrivate) {
            return false;
        }
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.city, other.city)) {
            return false;
        }
        if (!Objects.equals(this.builder, other.builder)) {
            return false;
        }
        if (!Objects.equals(this.country, other.country)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "település neve: " + city
                + "\népítész neve: " + builder
                + "\nország: " + country
                + "\nszobák száma: " + nrOfRooms
                + "\nmagánkézben van-e: " + isPrivate
                + "\nID: " + id + "\n\n";
    }
}