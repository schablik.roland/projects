package castle.comparator;

import java.io.Serializable;
import java.util.Comparator;

public class CastleCityComparator implements Comparator<Castle>, Serializable{
    
    @Override
    public int compare(Castle o1, Castle o2) {
        return o1.getCity().compareTo(o2.getCity());
    }
}
